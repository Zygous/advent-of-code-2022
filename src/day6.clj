(ns day6
  "Advent of Code 2022 - Day 6"
  (:require [clojure.set :as set]
            [clojure.string :as s]
            [util :as u]))

(defn search [windows]
  (let [size (count (first windows))]
    (reduce (fn [n window]
              (let [w (set window)]
                (if (= (count w) size)
                  (reduced n)
                  (inc n))))
            size
            windows)))

(defn -main [& _]
  (let [buffer (slurp "./input-6")
        p-offset (search (partition 4 1 buffer))
        m-offset (search (partition 14 1 buffer))]
    (println "Packet offset:" p-offset)
    (println "Message offset:" m-offset)))
