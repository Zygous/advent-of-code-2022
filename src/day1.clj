(ns day1
 "Advent of Code 2022 - Day 1"
 (:require [clojure.string :as s]))

(defn calc-pack-cals [pack]
  (->> pack
       (s/split-lines)
       (map #(Integer/parseInt %))
       (apply +)))

(defn sum-top-3-cals [pack-cals]
  (->> pack-cals
       (sort #(- %2 %1))
       (take 3)
       (apply +)))

(defn -main [& _]
  (let [packs (s/split (slurp "./input-1") #"\n\n")
        pack-cals (map calc-pack-cals packs)
        top-pack-cals (apply max pack-cals)
        top-3-cals (sum-top-3-cals pack-cals)]
   (println (str "Max food pack calories: " top-pack-cals))
   (println (str "Sum of top 3 pack calories: " top-3-cals))))
