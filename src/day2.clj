(ns day2
 "Advent of Code 2022 - Day 2"
 (:require [clojure.string :as s]))

(def char-moves {"A" :rock "B" :paper "C" :scissors "X" :rock "Y" :paper "Z" :scissors})
(def char-result {"X" :loss "Y" :draw "Z" :win})
(def move-scores {:rock 1 :paper 2 :scissors 3})
(def result-scores {:draw 3 :win 6 :loss 0})

(defn play [them us]
  (cond
    (= them us) :draw
    (= them :rock) (if (= us :paper) :win :loss)
    (= them :paper) (if (= us :scissors) :win :loss)
    (= them :scissors) (if (= us :rock) :win :loss)))

(defn play-back [them result]
  (cond
    (= result :draw) them
    (= result :win) (cond
                      (= them :paper) :scissors
                      (= them :rock) :paper
                      :else :rock)
    (= result :loss) (cond
                       (= them :paper) :rock
                       (= them :rock) :scissors
                       :else :paper)))

(defn calc-score-pt1 [round]
  (let [round (s/split round #" ")
        [them us] (map #(char-moves %) round)
        result (play them us)]
    (+ (move-scores us) (result-scores result))))

(defn calc-score-pt2 [round]
  (let [[them result] (s/split round #" ")
        them (char-moves them)
        result (char-result result)
        us (play-back them result)]
    (+ (move-scores us) (result-scores result))))

(defn -main [& _]
  (let [rounds (s/split-lines (slurp "./input-2"))
        pt1-scores (map calc-score-pt1 rounds)
        pt2-scores (map calc-score-pt2 rounds)]
    (println (str "Part 1 score: " (apply + pt1-scores)))
    (println (str "Part 2 score: " (apply + pt2-scores)))))
