(ns day3
  "Advent of Code 2022 - Day 3"
  (:require [clojure.set :as set]
            [util :as u]))

(defn char->score [c]
  (let [n (int c)
        ;; Re-index to begin at 1
        ;; Swap relative order of a-z and A-Z
        ;; Different offsets for upper and lower because
        ;;   of ASCII punctuation between Z and a
        offset (if (>= (int \Z) n)
                 (- (int \A) 27) ;; Caps start at 27
                 (- (int \a) 1)) ;; Lower-case starts at 1
        score (- n offset)]
    score))

(defn find-rogue-item [sack]
  (let [size (/ (count sack) 2)
        compartments (partition size sack)
        [a b] (map set compartments)
        rogue (first (set/intersection a b))
        score (char->score rogue)]
    score))

(defn find-group-badge-item [sacks]
  (let [[a b c] (map set sacks)
        badge (first (set/intersection a b c))
        score (char->score badge)]
    score))

(defn -main [& _]
  (let [sacks (u/file-lines "input-3")
        rogues (map find-rogue-item sacks)
        groups (partition 3 sacks)
        badge-items (map find-group-badge-item groups)]
    (println "Rogue items sum:" (apply + rogues))
    (println "Badge items sum:" (apply + badge-items))))
