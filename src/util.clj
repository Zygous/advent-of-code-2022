(ns util
  "Utils"
  (:require [clojure.string :as s]))

(defn file-lines
  "Reads the file at path, returning a vector of lines."
  [path]
  (s/split-lines (slurp path)))
