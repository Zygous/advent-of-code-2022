(ns day5
  "Advent of Code 2022 - Day 5"
  (:require [clojure.set :as set]
            [clojure.string :as s]
            [util :as u]))

(defn delim [s]
  (s/replace s #" {1,4}" ","))

(defn strip-brackets [s]
  (s/replace s #"[\[\]]" ""))

(defn comma-split [s]
  (s/split s #"," -1))

(defn parse-instruction [s]
  (let [[_ n _ from _ to] (s/split s #" ")
        [n from to] (map #(Integer/parseInt %) [n from to])]
    [n (dec from) (dec to)])) ; 0-index

(defn move-crate [from to]
  (let [crate (last from)
        from (drop-last from)
        to (conj (vec to) crate)]
    [from to]))

(defn move-crates [n from to]
  (let [from (vec from)
        to (vec to)
        crates (subvec from (- (count from) n))
        from (drop-last n from)
        to (apply conj to crates)]
    [from to]))

(defn restack-1 [stacks instructions]
  (reduce (fn [stacks instruction]
            (let [[n src dest] (parse-instruction instruction)
                  from (nth stacks src)
                  to (nth stacks dest)
                  [from to] (reduce (fn [[from to] _n]
                                      (move-crate from to))
                                    [from to]
                                    (range n))
                  ]
              (-> stacks
                  (assoc src from)
                  (assoc dest to))))
          (vec stacks)
          instructions))

(defn restack-2 [stacks instructions]
  (reduce (fn [stacks instruction]
            (let [[n src dest] (parse-instruction instruction)
                  from (nth stacks src)
                  to (nth stacks dest)
                  [from to] (move-crates n from to)]
              (-> stacks
                  (assoc src from)
                  (assoc dest to))))
          (vec stacks)
          instructions))

(defn top-crates [stacks]
  (apply str
         (reduce (fn [acc stack] (conj acc (last stack)))
                 []
                 stacks)))

(defn -main [& _]
  (let [lines (u/file-lines "input-5")
        crate-lines (take-while (comp not empty?) lines)
        instructions (nthrest lines (inc (count crate-lines)))
        crate-lines (rest (reverse crate-lines))
        crates (map (comp comma-split strip-brackets delim) crate-lines)
        stacks (apply (partial mapv vector) crates)
        stacks (map (partial filter (comp not empty?)) stacks)
        final-stacks-1 (restack-1 stacks instructions)
        final-stacks-2 (restack-2 stacks instructions)]
    (println (str "9000 top crates: " (top-crates final-stacks-1)))
    (println (str "9001 top crates: " (top-crates final-stacks-2)))))

