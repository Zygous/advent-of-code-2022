(ns day7
  "Advent of code 2022 - Day 7"
  (:require [clojure.string :as s]
            [util :as u]))

(defn calc-dir-sizes [lines]
  (reduce (fn [{:keys [pwd fs] :as state} line]
            (cond
              ;; Change up one level
              (= line "$ cd ..")
              {:pwd (vec (drop-last pwd)) :fs fs}

              ;; Line show file size
              (Character/isDigit (first line))
              (let [[size _] (s/split line #" ")
                    size (Integer/parseInt size)]
                {:pwd pwd
                 :fs (:fs (reduce
                       (fn [acc p]
                         (let [path (conj (:path acc) p)
                               _ (clojure.pprint/pprint path)
                               acc (assoc acc :path path)
                               acc (update-in acc [:fs path] (partial + size))]
                           acc))
                       {:fs fs :path []}
                       pwd))})

              ;; don't need to do anything for dir lines
              (= (subs line 0 3) "dir")
              state

              ;; don't need to do anything for ls commands
              (= line "$ ls")
              state

              ;; Change down one level
              (= (subs line 0 4) "$ cd")
              (let [name (subs line 5)
                    path (conj pwd name)]
                {:pwd path
                 :fs (if (nil? (get fs path)) (assoc fs path 0) fs)})))
          {:pwd [] :fs {}}
          lines))

(defn total-size [fs]
  (clojure.pprint/pprint fs)
  (apply +
         (filter
           (partial <= 100000)
           (vals fs))))

(defn -main [& _]
  (let [lines (u/file-lines "input-7")
        info (calc-dir-sizes lines)]
    (println "Part1: " (total-size (:fs info)))))

(-main)

(comment
  (subs "$ cd /" 5)
  (first "135774 pub")
         (hash-map )
         (apply + (vals {:one 1 :two 2}))
  )
