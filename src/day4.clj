(ns day4
  "Advent of Code 2022 - Day 4"
  (:require [clojure.set :as set]
            [clojure.string :as s]
            [util :as u]))

(defn parse-ranges [s]
  (let [bounds (s/split s #"[-,]" 4)
        [llo lhi rlo rhi] (map #(Integer/parseInt %) bounds)
        l (set (range llo (inc lhi)))
        r (set (range rlo (inc rhi)))
        ln (count l)
        rn (count r)
        intersect? (not (empty? (set/intersection l r)))
        eclipse? (if (> ln rn)
                         (set/subset? r l)
                         (set/subset? l r))]
    {:eclipse? eclipse? :intersect? intersect?}))

(defn -main [& _]
  (let [lines (u/file-lines "input-4")
        range-pairs (map parse-ranges lines)
        overlapped (filter :eclipse? range-pairs)
        intersections (filter :intersect? range-pairs)]
    (println (str "Number of totally overlapped ranges: "
                  (count overlapped)))
    (println (str "Number of intersected ranges: "
                  (count intersections)))))

